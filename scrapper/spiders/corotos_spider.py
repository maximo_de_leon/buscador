from scrapy.spiders import SitemapSpider
from scrapper.items import CorotosItem

class CorotosSpider(SitemapSpider):
    name = "corotos"
    sitemap_urls = ['http://www.corotos.com.do/robots.txt']
    
    def parse (self, response):
        for sel in response.xpath('//div[@class="span13"]/div[@class="panel relative panel-main panel-noheading"]'):
            item = CorotosItem()
            item['title'] = sel.xpath('div[@class="panel-body"]\
    			/div[@class="page-header-wrap"]/h1[@class="page-header mtxs mbxs"]/text()').extract()
            item['desc'] = sel.xpath('div[@class="panel-body mbl"]/div[@class="fs16"]/p/text()').extract()
            item['desc_html'] = sel.xpath('div[@class="panel-body mbl"]/div[@class="fs16"]').extract()
            item['link'] = response.url
            item['precio'] = sel.xpath('h2[@class="vi-price-label text-center fs18 no-margin"]/span/text()').extract()
            item['image'] = sel.xpath('div[@class="carousel slide mbm mts"]/div[@class="carousel-wrap text-center"]\
                                      /div[@class="carousel-inner"]/div[@class="item active"]/img/@src').extract()
            item['domain_name'] = self.name
            yield item