import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.sgml import SgmlLinkExtractor
from scrapper.items import EmarketItem
import re

class EmarketSpider(CrawlSpider):
    name="emarket"
    allowed_domains= ['emarket.do']
    pattern = re.compile('\\.\d+')
    start_urls=['http://www.emarket.do/categorias.html/']
    rules = (Rule(SgmlLinkExtractor(allow=(), deny=('login/registro\.html','inicio/comprar\.html#consejos', \
		'inicio/comprar\.html#aprende', 'anuncio/crear\.html', 'inicio/vender\.html#aprende', 'tienda/bienvenido\.html', \
		'inicio/metodos-de-pago\.html', 'inicio/faqs\.html', 'inicio/metodos-de-pago\.html#online', \
		'inicio/metodos-de-pago\.html#paypal', 'inicio/metodos-de-pago\.html#deposito','inicio/planes\.html', 'tiendas\.html', \
		'inicio/banners\.html', 'inicio/politicas\.html', 'anuncio/crear\.html', 'perfil/')) \
		, callback='parse_item', follow=True),)

    def parse_item(self, response):
        item = EmarketItem()
        if 'anuncio' in response.url:
            item['link'] = response.url
            item['title'] = response.selector.xpath('//div[@class="content"]/div[@class="row"]/div/div[@class="span10 show-post"]\
 				/div/div/div[@class="span6"]/div[@class="well well-small post-title"]/h4/text()').extract()
            item['desc'] = response.xpath('//div[@class="content"]/div[@class="row"]/div/div[@class="span10 show-post"]/div \
 				/div[@class="span10"]/div/div[@class="box-content"]/text()').extract()
            item['desc_html'] = response.xpath('//div[@class="content"]/div[@class="row"]/div/div[@class="span10 show-post"]/div \
                /div[@class="span10"]/div/div[@class="box-content"]').extract()
            precio = response.xpath('//div[@class="content"]/div[@class="row"]/div/div[@class="span10 show-post"]\
                /div/div/div[@class="span6"]/div[@class="row"]/div[@class="span6"]/ \
                div[@class="well well-small well-price"]/h4/span/text()').extract()
            item['precio'] = self.pattern.sub('', precio[0].strip())
            item['image'] = response.xpath('//div[@class="content"]/div[@class="row"]/div/div[@class="span10 show-post"]\
 				                            /div/div/div[@class="span4"]/div/div[@class="carousel-inner"] \
                                           /div[@class="item active"]/a/img/@src').extract()
            item['domain_name'] = self.name
        return item

    def parse_start_url(self, response):
        print self.parse_item(response)