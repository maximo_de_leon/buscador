import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.sgml import SgmlLinkExtractor
from scrapper.items import LapulgaItem
import re

class LapulgaSpider(CrawlSpider):
    name="lapulga"
    allowed_domains= ['lapulga.com.do']
    pattern = re.compile('\\.\d+')
    start_urls=['http://www.lapulga.com.do/categorias']
    rules = (Rule(SgmlLinkExtractor(allow=(), deny=('/dealer','/empleos','/servicios','/prestamos\.html',\
		'/tiendas', '/inmobiliarias','/lista-de-espera-vip\.html','/publicarempleo\.php','/publicarservicio\.php', \
		'/publicarvideo\.php','/registro\.php', '/soporte\.php?ai=co','/politicas\.php','/videos\.html', '/rss\.php', \
		'/publica\.php', '/mipulga\.php','/mregistro\.php', '/planes\.php', '/pines\.php','/micuenta\.php', '/buzon\.php', \
		'/login\.php')
		) , callback='parse_item', follow=True),)

    def parse_item(self, response):
        item = LapulgaItem()
        if '.html' in response.url:
            item['link'] = response.url
            item['desc'] = response.xpath('//div[@id="wrap"]/div[@class="descl"]/text()').extract()
            item['desc_html'] = response.xpath('//div[@id="wrap"]/div[@class="descl"]').extract()
            item['title'] = response.xpath('//div[@id="wrap"]/div[@id="iart"]/h1/text()').extract()
            precio = response.xpath('//div[@id="wrap"]/div[@id="iart"]/div[@class="binfoart"]\
                                                    /ul/li/span/span[@class="preciod"]/text()').extract()
            item['precio'] = self.pattern.sub('', precio[0].strip())
            item['image'] = 'http://www.' + self.allowed_domains[0] + self.allowed_domains[0].join(
                            str(i).encode('ascii', 'ignore') \
                            for i in response.xpath('//div[@id="wrap"]/div[@id="galeria"]/p/a/img/@src').extract())\
                            .encode('ascii', 'ignore')
            item['domain_name'] = self.name
        return item

    def parse_start_url(self, response):
        print self.parse_item(response)