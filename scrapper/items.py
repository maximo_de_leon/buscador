# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class CorotosItem(scrapy.Item):
	title = scrapy.Field()
	link = scrapy.Field()
	desc = scrapy.Field()
	desc_html = scrapy.Field()
	image = scrapy.Field()
	precio = scrapy.Field()
	domain_name = scrapy.Field()

class EmarketItem(scrapy.Item):
	title = scrapy.Field()
	link = scrapy.Field()
	desc = scrapy.Field()
	desc_html = scrapy.Field()
	image = scrapy.Field()
	precio = scrapy.Field()
	domain_name = scrapy.Field()

class LapulgaItem(scrapy.Item):
	title = scrapy.Field()
	link = scrapy.Field()
	desc = scrapy.Field()
	desc_html = scrapy.Field()
	image = scrapy.Field()
	precio = scrapy.Field()
	domain_name = scrapy.Field()