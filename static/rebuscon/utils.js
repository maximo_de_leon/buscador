/**
 * Created by mgalan on 04/11/2015.
 */

$.ajaxSetup({ cache: false, headers: { "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val() } });

$(document).ajaxStart(function () {
    $('.loadingMask').fadeIn({ duration: 1 });
    //$("#tooltip").css('display', 'none');
}).ajaxStop(function () {
    $('.loadingMask').fadeOut({ duration: 1 });
}).ajaxError(function (event, jqxhr, settings, thrownError) { //NO SONAR
    if (jqxhr && jqxhr.responseText && jqxhr.responseText.length > 0)
        showErrorAlert(jqxhr.responseText);
    else
        showNotification("Ha ocurrido un error en el servidor", notificationType.DANGER, false, true);
});
