/**
 * Created by mgalan on 03/11/2015.
 */
angular.module('RebusconApp').controller('SearchCtrl', ['$scope', '$sce', function ($scope, $sce) {
    $scope.searchBox = "";
    $scope.colores = colores;
    $scope.results = [];
    $scope.pages = [];
    $scope.total = 0;
    $scope.currentPage = 0;
    $scope.search = function (page, s) {
        var postObj = {"q": s || $scope.searchBox};
        if (typeof page !== 'undefined') {
            postObj["page"] = page;
            $scope.currentPage = page;
            var pages = $scope.pages;
            if (pages[pages.length - 1].value > 2) {
                $scope.pages = [];
                $scope.pages.push(pages[0]);
                for (var i = 1; i < pages[pages.length - 1].value; i++) {
                    if (i < $scope.currentPage - 1) {
                        i = $scope.currentPage - 2;
                        $scope.pages.push({value: i, text: "..."});
                    } else if (i > $scope.currentPage + 1) {
                        $scope.pages.push({value: $scope.currentPage + 2, text: "..."});
                        i = pages[pages.length - 1].value;
                    } else {
                        $scope.pages.push({value: i, text: i + 1});
                    }
                }
                $scope.pages.push(pages[pages.length-1]);
            }
        }
        $.post(searchUrl, postObj, function (data) {
            $scope.results = data.results;
            $scope.total = data.total;
            if (typeof page === 'undefined') {
                $scope.currentPage = 0;
                $scope.pages = [];
                if (data.numPages) {
                    for (var i = 0; i < data.numPages; i++) {
                        if (i < 2) {
                            $scope.pages.push({value: i, text: i + 1});
                        } else if (i == 2 && data.numPages > 3) {
                            $scope.pages.push({value: i, text: "..."});
                        } else {
                            $scope.pages.push({value: data.numPages - 1, text: data.numPages});
                            i = data.numPages;
                        }
                    }
                }
            }
            $scope.$apply();

            $('[item_desc]').each(function(index){
                var contentHtml = $($(this).next().html());
                $(this).tooltipster({
                    theme: 'tooltipster-shadow',
                    content: contentHtml
                });
            });
        });
    };

    // Esta es la busqueda para el index
    $scope.search(undefined, "__INDEX__");
}]);