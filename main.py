from flask import Flask, render_template, request, jsonify
import solr

# configuration
SOLR_URL = 'http://localhost:8983/solr/gettingstarted_shard1_replica1'

COLOR_COROTOS = '#6CB3EF'
COLOR_EMARKET = '#E07314'
COLOR_LAPULGA = '#232F3E'

app = Flask(__name__)


@app.route('/')
def index():
    colors = {'corotos': COLOR_COROTOS, 'emarket': COLOR_EMARKET, 'lapulga': COLOR_LAPULGA}
    return render_template('index.html', colores=colors)


@app.route('/buscar', methods=['POST'])
def buscar():
    s = solr.SolrConnection(SOLR_URL)
    sbox = request.form['q']

    results = None
    if sbox == "__INDEX__":
        s = solr.SolrConnection(SOLR_URL)
        resultsEmarket = s.query('domain_name:*emarket*', rows=5)
        resultsPulga = s.query('domain_name:*lapulga*', rows=5)
        resultsCorotos = s.query('domain_name:*corotos*', rows=5)
        results = resultsCorotos.results + resultsEmarket.results + resultsPulga.results
        return jsonify(results=remove_unicode(results))
    else:
        start = 0
        if 'page' in request.form:
            start = int(request.form['page']) * 9

        results = s.query('*' + sbox.encode('ascii', 'ignore') + '*', rows=9, start=start)

        return jsonify(q=sbox,
                   results=remove_unicode(results),
                   total=results.numFound,
                   numPages=results.numFound / 9 + (1 if results.numFound % 9 > 0 else 0))


def remove_unicode(lista):
    result = []
    for item in lista:
        title = ''
        link = ''
        desc = ''
        desc_html = ''
        image = ''
        precio = ''
        domain_name = ''
        if 'title' in item and item['title'] != None:
            title = item['title'][0].encode('ascii', 'ignore')
        if 'link' in item and item['link'] != None:
            link = item['link'][0].encode('ascii', 'ignore')
        if 'desc' in item and item['desc'] != None:
            desc = item['desc'][0].encode('ascii', 'ignore')
        if 'desc_html' in item and item['desc_html'] != None:
            desc_html = item['desc_html'][0]
        if 'image' in item and item['image'] != None:
            image = item['image'][0].encode('ascii', 'ignore')
        if 'precio' in item and item['precio'] != None:
            precio = item['precio'][0]
        if 'domain_name' in item and item['domain_name'] != None:
            domain_name = item['domain_name'][0].encode('ascii', 'ignore')
        result.append(
            {'title': title, 'link': link, 'desc': desc, 'desc_html': desc_html, 'image': image, 'precio': precio, 'domain_name': domain_name})

    return result


if __name__ == '__main__':
    app.run(debug=True)
